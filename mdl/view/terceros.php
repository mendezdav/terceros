<?php

class tercerosView{
    public function principal(){
        template()->buildFromTemplates('template_nofixed.html');
        page()->setTitle('Módulo de gestión de terceros');
        template()->addTemplateBit('content', 'terceros/principal.html');
        page()->addEstigma("username", Session::singleton()->getUser());
        page()->addEstigma("TITULO", "Página principal");
        page()->addEstigma("back_url", '/'.MODULE.'/terceros/principal');
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
	}
    
    public function proveedores(){
        template()->buildFromTemplates('template_nofixed.html');
        page()->setTitle('Registrar proveedor');
        template()->addTemplateBit('content', 'terceros/proveedores.html');
        page()->addEstigma("username", Session::singleton()->getUser());
        page()->addEstigma("TITULO", "Registrar nuevo proveedor");
        page()->addEstigma("back_url", '/'.MODULE.'/terceros/principal');
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
    }
    
    public function consulta_proveedores(){
        template()->buildFromTemplates('template_nofixed.html');
        page()->setTitle('Ficha de proveedor');
        template()->addTemplateBit('content', 'terceros/consulta_proveedores.html');
        page()->addEstigma("username", Session::singleton()->getUser());
        page()->addEstigma("TITULO", "Consultar proveedor");
        page()->addEstigma("back_url", '/'.MODULE.'/terceros/principal');
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
    }
    
    public function lista_proveedores($cache){
        $dias = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        template()->buildFromTemplates('template_nofixed.html');
        page()->setTitle('Lista de proveedores');
        template()->addTemplateBit('content', 'terceros/lista_proveedores.html');
        page()->addEstigma("username", Session::singleton()->getUser());
        page()->addEstigma("TITULO", "Mis proveedores");
        page()->addEstigma("proveedores", array('SQL', $cache[0]));
        page()->addEstigma("{c_1}", $dias[0]);
        page()->addEstigma("{c_2}", $dias[1]);
        page()->addEstigma("{c_3}", $dias[2]);
        page()->addEstigma("{c_4}", $dias[3]);
        page()->addEstigma("{c_5}", $dias[4]);
        page()->addEstigma("{c_6}", $dias[5]);
        page()->addEstigma("{c_7}", $dias[6]);
        page()->addEstigma("back_url", '/'.MODULE.'/terceros/principal');
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
    }
    
    public function terceros(){
        template()->buildFromTemplates('template_nofixed.html');
        page()->setTitle('Registrar proveedor');
        template()->addTemplateBit('content', 'terceros/terceros.html');
        page()->addEstigma("username", Session::singleton()->getUser());
        page()->addEstigma("TITULO", "Mantenimiento a terceros");
        page()->addEstigma("back_url", '/'.MODULE.'/terceros/principal');
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
    }
}
	
?>