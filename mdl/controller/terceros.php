<?php

import('mdl.model.terceros');
import('mdl.view.terceros');

class tercerosController extends controller{
	public function principal(){
		
		$this->view->principal();
	}	
	
	public function proveedores(){
		
		$this->view->proveedores();
	}
	
	public function consulta_proveedores(){
		
		$this->view->consulta_proveedores();
	}
	
	public function lista_proveedores(){
		$cache = array();
		$cache[0] = $this->model->get_child('proveedor')->get_list();
		$this->view->lista_proveedores($cache);
	}
	
	public function get_data_proveedores(){
		$params = json_decode(file_get_contents('php://input'),true);
		if(!empty($params)){
			if(!empty($params['codigo'])){
				$response = array();
				$codigo   = $params['codigo'];
				$provOb   = $this->model->get_child('proveedor');
				
				$response['exists'] = false;
				
				if($provOb->exists($codigo)){
					
					$response['exists'] = true;	
					$provOb->get($codigo);
					$fields = $provOb->get_fields();
					foreach($fields as $field){
						$response[$field] = $provOb->$field;
					}
					
					$casascredito = $this->model->get_child('casascredito');
					$casascredito->get($response['banco']);
					$response['banco'] = $casascredito->casa;
				}
				
				echo json_encode($response);
			}
		}
	}
	
	public function terceros(){
		
		$this->view->terceros();
	}
	
	public function guardar_proveedor(){
		
		$proveedor = $this->model->get_child('proveedor');
		$proveedor->get(0);
		$proveedor->change_status($_POST);
		
		$proveedor->save();
		
		HttpHandler::redirect('/terceros/terceros/proveedores?status=save');
	}
	
	public function guardar_tercero(){
		
		$proveedor = $this->model->get_child('cliente');
		$proveedor->get(0);
		$proveedor->change_status($_POST);
		
		$proveedor->save();
		
		HttpHandler::redirect('/terceros/terceros/terceros?status=save');
	}
}	
	
?>