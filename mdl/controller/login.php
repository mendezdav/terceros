<?php

import('mdl.view.login');
import('mdl.model.login');
proveedor_activo();

class LoginController extends controller {

    public function form() {
        if (!Session::singleton()->ValidateSession()) {
            $this->view->show_form();
        } else {
            if(!isset($_GET['redirectTo'])||empty($_GET['redirectTo'])){
                HttpHandler::redirect('/'.MODULE.'/terceros/principal');
            }else{
                HttpHandler::redirect($_GET['redirectTo']);
            }
        }
    }

    public function info() {
        $this->view->show_info();
    }

    public function login() {
        if (empty($_POST)) {
            HttpHandler::redirect('/'+MODULE+'/login/form');
        } else {
            BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, DATABASE);
            $usuario = BM::singleton()->getObject('db')->sanitizeData($_POST['usuario']);
            $clave = cifrar_RIJNDAEL_256($_POST['clave']);
            $query = "SELECT * FROM empleado WHERE usuario='{$usuario}' AND clave='{$clave}' AND modulo='terceros';";
            BM::singleton()->getObject('db')->executeQuery($query);
            if (BM::singleton()->getObject('db')->getNumRows() > 0) {
                $level = 1;
                while ($data = BM::singleton()->getObject('db')->getResult()->fetch_assoc()) {
                    $level = $data['permiso'];
                }
                Session::singleton()->NewSession($usuario, $level);
                if(!isset($_GET['url'])||empty($_GET['url'])){
                    HttpHandler::redirect('/'.MODULE.'/login/form');
                }else{
                    HttpHandler::redirect($_GET['url']);
                }
            } else {
                HttpHandler::redirect('/'.MODULE.'/login/form?error_id=2');
            }
        }
    }

}

?>
